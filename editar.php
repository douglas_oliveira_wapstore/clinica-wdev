<?php 

require __DIR__.'./vendor/autoload.php';

use \App\Entity\Especialidade;
use \App\Entity\Medico;

use \App\Entity\Clinica;

//MENSAGENS DA PÁGINA
$msgHeader = 'Editar produto';
$msgBotao = 'Editar';

//OBTÉM TODAS AS ESPECIALIDADES DO BANCO
$especialidades = Especialidade::getEspecialidades();

//OBTÉM TODAS AS CLÍICAS DO BANCO  
$clinicas = Clinica::getClinicas();

if(!isset($_GET['id'])||!is_numeric($_GET['id'])) {
    header('location: index.php?status=error');
    exit;
}

if (isset($_GET['id'])) {
    $obMedico = Medico::getmedbyid($_GET['id']);

}

if(!$obMedico instanceof Medico) {
    header('location: index.php?status=error');
    exit;
}


if(isset($_POST['enviar'])) {
    if(isset($_POST['nome'], $_POST['crm'], $_POST['idade'], $_POST['especialidade'], $_POST['clinica'], $_POST['sexo'])) {
        $obMedico = new Medico();

        $obMedico->nome = $_POST['nome'];
        $obMedico->crm = $_POST['crm'];
        $obMedico->idade = $_POST['idade'];
        $obMedico->idespecialidade = $_POST['especialidade'];
        $obMedico->idclinica = $_POST['clinica'];
        $obMedico->sexo = $_POST['sexo'];


        $obMedico->atualizar($_GET['id']);


        header("location: index.php?status=success");
        exit;
    }
}



include __DIR__.'./includes/form-cad-medico.php';

?>