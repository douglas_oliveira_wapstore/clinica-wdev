# Clínica WDEV

O desenvolvimento deste projeto se constitui um dos instrumentos de avaliação para ingresso na web.art group como Programador Back-end Assistente. O intuito do projeto é a criação de um CRUD básico.

## Getting started

* Para o funcionamento do site, será necessário executar os Scripts SQL contidos no arquivo [clinica.sql](https://gitlab.com/douglas_oliveira_wapstore/clinica-wdev/-/blob/main/clinica.sql);

* Logo após, será necessário executar o comando *"composer install"* para importação de dependências utilizadas pelo projeto.

* Por fim, basta aproveitar!
