<?php 

require __DIR__.'./vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Medico;
use \App\Entity\Especialidade;

use \App\Entity\Clinica;


//BUSCA DO FILTRO
$nome = filter_input(INPUT_GET, 'nome', FILTER_SANITIZE_STRING);

$querybusca = 'nome LIKE "%'.str_replace(" ",'%',$nome).'%"';


//OBTÉM TODOS OS MÉDICOS DO BANCO DE DADOS
$obMeds = Medico::getMeds($querybusca ?? null, 'nome');





include __DIR__.'./includes/listagem-medicos.php';

?>