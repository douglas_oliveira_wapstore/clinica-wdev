create database clinica_wdev;

use clinica_wdev;

create table medico (

	id integer not null auto_increment primary key,
    crm varchar(255) not null unique,
    nome varchar(255) not null,
    idade varchar(20) not null,
    sexo varchar(15) not null,
    idespecialidade integer not null,
    idclinica integer not null,
    
    FOREIGN KEY(idespecialidade) REFERENCES especialidade(idespecialidade),
    FOREIGN KEY(idclinica) REFERENCES clinica(idclinica)


);

create table especialidade (

	idespecialidade integer not null primary key auto_increment,
    nome varchar(255) not null

);

create table clinica (

	idclinica integer not null primary key auto_increment,
    nome varchar(255) not null

);

alter table clinica add endereco varchar(255) not null;

alter table especialidade add descricao varchar(255) not null;

drop table medico;

select * from clinica;

select * from especialidade;

select * from medico;