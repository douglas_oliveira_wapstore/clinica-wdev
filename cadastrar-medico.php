<?php 

require __DIR__.'./vendor/autoload.php';

use \App\Entity\Especialidade;
use \App\Entity\Medico;

use \App\Entity\Clinica;

//OBTÉM TODAS AS ESPECIALIDADES DO BANCO
$especialidades = Especialidade::getEspecialidades();

//OBTÉM TODAS AS CLÍICAS DO BANCO  
$clinicas = Clinica::getClinicas();

$msgHeader = 'Cadastrar produto';
$msgBotao = 'Cadastrar';


if(isset($_POST['enviar'])) {
    if(isset($_POST['nome'], $_POST['crm'], $_POST['idade'], $_POST['especialidade'], $_POST['clinica'], $_POST['sexo'])) {
        $obMedico = new Medico();

        $obMedico->nome = $_POST['nome'];
        $obMedico->crm = $_POST['crm'];
        $obMedico->idade = $_POST['idade'];
        $obMedico->idespecialidade = $_POST['especialidade'];
        $obMedico->idclinica = $_POST['clinica'];
        $obMedico->sexo = $_POST['sexo'];


        $obMedico->cadastrar();


        header("location: index.php?status=success");
        exit;
    }
}



include __DIR__.'./includes/form-cad-medico.php';

?>