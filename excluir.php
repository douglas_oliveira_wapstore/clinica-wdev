<?php 

require __DIR__.'./vendor/autoload.php';

use \App\Db\Database;
use \App\Entity\Medico;
use \App\Entity\Especialidade;

use \App\Entity\Clinica;

//FAZ AS VERIFICAÇÕES DA URL
if(!isset($_GET['id'])||!is_numeric($_GET['id'])) {
    header('location: index.php?status=error');
    exit;
}

if (isset($_GET['id'])) {
    $obMedico = Medico::getmedbyid($_GET['id']);

}

if(!$obMedico instanceof Medico) {
    header('location: index.php?status=error');
    exit;
}


//EXCLUIR O PRODUTO
if(isset($_POST['excluir'])) {

    Medico::apagar($_GET['id']);

    header('location: index.php?status=success');
    exit;
}

include __DIR__.'./includes/confirma-exclusao.php';

?>