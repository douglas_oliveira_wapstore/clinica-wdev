<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CLínica WDEV</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>
<body>


    <div class="container">
    
    
        <div class="jumbotron p-4 bg-light">
        
            <h1 class="display-4">Clínica WDEV</h1>

            <hr class="my-4">

            <p class="stem">Seja bem-vindo!</p>

        </div>
    
    </div>

    <div class="container">

        <h1 class="display-4 my-4" style="font-size: 35px; text-align: center;">Deseja excluir <b><?=$obMedico->nome?></b> ? </h1>
    
        <form method="post" class="form" style="width: 50vw; margin: 0 auto; text-align:center;">
        
            
            <button class="btn btn-danger" name="excluir">Excluir</button>
            <a href="index.php"><button type="button" class="btn btn-primary">Cancelar</button></a>
        
        
        </form>
    
    
    
    </div>
    

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>