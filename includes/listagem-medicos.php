<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CLínica WDEV</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>


<?php 

use \App\Db\Database;
use \App\Entity\Medico;
use \App\Entity\Especialidade;

use \App\Entity\Clinica;

$listagemmedicos = '';


foreach($obMeds as $med) {

    $nome_esp = Especialidade::getespbyid($med->idespecialidade)->nome;
    $nome_clin = Clinica::getclinbyid($med->idclinica)->nome;


    $listagemmedicos .= '
    
        <tr>
            <td>'.$med->id.'</td>
            <td>'.$med->nome.'</td>
            <td>'.$med->crm.'</td>
            <td>'.$nome_esp.'</td>
            <td>'.$nome_clin.'</td>
            <td><a href="editar.php?id='.$med->id.'"><button type="button" class="btn btn-outline-dark btn-sm"> Editar </button></a>
                <a href="excluir.php?id='.$med->id.'"><button type="button" class="btn btn-outline-danger btn-sm"> Excluir </button></a>
            
        </tr>
    ';
}



$listagemmedicos = (!strlen($listagemmedicos)) ? $listagemmedicos = '<td colspan="6"> Nenhum médico encontrado </td>' : $listagemmedicos;




?>


<body>


    <div class="container">
    
    
        <div class="jumbotron p-4 bg-light">
        
            <h1 class="display-4">Clínica WDEV</h1>

            <hr class="my-4">

            <p class="stem">Seja bem-vindo!</p>

            <a href="cadastrar-medico.php"><button class="btn btn-outline-dark">Cadastrar médico</button></a>
            <a href="cadastrar-clinica.php"><button class="btn btn-outline-dark">Cadastrar clínica</button></a>
            <a href="cadastrar-especialidade.php"><button class="btn btn-outline-dark">Cadastrar especialidade</button></a>
        
        </div>
    
    </div>

    <form method="get" class="form">
    
        <div class="form-group" style="display: flex;width: 70vw; margin: 0 auto;">

            <input type="text" name="nome" class="form-control" placeholder="Filtre pelo nome:" value="<?=$nome ?? ''?>">
            <button class="btn btn-primary" name="filtrar">Filtrar</button>


        </div>
    
    
    </form>

    <div class="container mt-4">
    
        <table class="table">
            <thead>
            
                <th>ID</th>
                <th>Nome</th>
                <th>CRM</th>
                <th>Especialidade</th>
                <th>Clínica</th>
                <th>Ações</th>

            </thead>

            <tbody>
            
                <?=$listagemmedicos?>
            
            </tbody>
        
        
        
        </table>


    
    </div>

    
    
    
    

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>