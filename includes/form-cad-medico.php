<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CLínica WDEV</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
</head>

<style>

    .form-group {
        margin-bottom: 15px;
    }

</style>


<?php 



$listagemespecialidades ='';

foreach($especialidades as $esp) {
    $listagemespecialidades .= '
            <option value="'.$esp->idespecialidade.'" '.( isset($obMedico->idespecialidade)&&$obMedico->idespecialidade == $esp->idespecialidade ? 'selected' : '' ).' > '.$esp->nome .'</option>';
}


$listagemeclinicas = '';

foreach($clinicas as $clin) {
    $listagemeclinicas .= '<option value="'.$clin->idclinica.'" '.( isset($obMedico->idclinica)&&$obMedico->idclinica == $clin->idclinica ? 'selected' : '' ).'> '.$clin->nome . '</option>';
}

?>

<body>


    <div class="container">
    
    
        <div class="jumbotron p-4 bg-light">
        
            <h1 class="display-4">Clínica WDEV</h1>

            <hr class="my-4">

            <p class="stem">Seja bem-vindo!</p>

        </div>
    
    </div>

    <div class="container">

        <h1 class="display-4 my-4" style="font-size: 35px; text-align: center;"><?=$msgHeader?></h1>
    
        <form method="post" class="form" style="width: 50vw; margin: 0 auto;">
        
            <div class="form-group">
            <label for="nome">Nome do médico:</label>
                <input type="text" class="form-control" name="nome" value="<?=$obMedico->nome ?? ''?>" required>
            </div>

                    
            <div class="form-group">
            <label for="crm">CRM:</label>
                <input type="text" class="form-control" value="<?=$obMedico->crm ?? ''?>" name="crm" required>
            </div>

            <div class="form-group">
                <label for="crm">Idade do médico:</label>
                <input type="text" class="form-control" name="idade" value="<?=$obMedico->idade ?? ''?>" required>
            </div>

            
            <div class="form-group">
                <input type="radio" name="sexo" class="form-check-input" value="masculino" <?=isset($obMedico->sexo)&&$obMedico->sexo == 'masculino' ? 'checked' : '' ?> required> Masculino
                <input type="radio" name="sexo" class="form-check-input" value="feminino"  <?=isset($obMedico->sexo)&&$obMedico->sexo == 'feminino' ? 'checked' : '' ?>  required> Feminino
                <input type="radio" name="sexo" class="form-check-input" value="nao_informado"  <?=isset($obMedico->sexo)&&$obMedico->sexo == 'nao_informado' ? 'checked' : '' ?> required> Prefiro não informar
            </div>

            <div class="form-group">
            
                            
            <label for="especialidade">Especialidade do médico</label>
                <select name="especialidade" id="" class="form-control" required>
                    <option value="" selected disabled>Selecione uma opção: </option>
                    <?=$listagemespecialidades?>
                </select>
            
            </div>

            <div class="form-group">
            
            <label for="clinica">Clínica que o médico trabalha</label>
            <select name="clinica" id="" class="form-control" required>
                <option value="" selected disabled>Selecione uma opção: </option>
                <?=$listagemeclinicas?>
            </select>
        
        </div>

            <button class="btn btn-primary" name="enviar"><?=$msgBotao?></button>
            <a href="index.php"><button type="button" class="btn btn-danger" >Voltar</button></a>


        
        
        </form>
    
    
    
    </div>
    

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
</body>
</html>