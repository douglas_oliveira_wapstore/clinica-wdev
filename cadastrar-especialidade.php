<?php 

require __DIR__.'./vendor/autoload.php';

use \App\Entity\Especialidade;

if(isset($_POST['enviar'])){
    if(isset($_POST['nome'], $_POST['descricao'])) {
        $obEspecialidade = new Especialidade();

        $obEspecialidade->nome = $_POST['nome'];
        $obEspecialidade->descricao = $_POST['descricao'];

        $obEspecialidade->cadastrar();
        

        header('location: index.php?status=success');
        exit;
    }

}

include __DIR__.'./includes/form-cad-especialidade.php';

?>