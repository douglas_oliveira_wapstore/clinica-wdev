<?php 

namespace App\Db;

use \PDO;
use \PDOException;


class Database {


    //HOST DE ACESSO
    const HOST = 'localhost';

    //NOME DO BANCO DE DADOS
    const DBNAME = 'clinica_wdev';

    //USUÁRIO DE ACESSO AO BANCO
    const USER = 'root';

    //SENHA DE ACESSO AO BANCO
    const PASSWD = '';

    //VARIÁVEL DE CONEXÃO COM O BANCO
    private $connection;

    //TABELA COM A QUAL SERÁ FEITA A CONEXÃO
    private $table;

    //CONSTRUTOR DA CLASSE
    public function __construct($table) {

        $this->table = $table;

        $this->setConnection();
    }

    //REALIZA A CONEXÃO COM O BANCO
    public function setConnection() {

        try {
            $this->connection = new PDO("mysql:host=".self::HOST.';dbname='.self::DBNAME,self::USER,self::PASSWD);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        } catch (PDOException $e) {
            echo 'Erro: '.$e;
        }


    }

    //EXECUTA OPERAÇÕES DO SQL
    public function execute($query, $param = []) {

        try {
            $stmt = $this->connection->prepare($query);
            $stmt->execute($param);

            return $stmt;
        } catch (PDOException $e) {
            echo "Error: ".$e;
        }
    }

    //INSERE DADOS DENTRO DO BANCO
    public function insert($array) {
        $values = array_values($array);
        $keys = array_keys($array);

        $binds = array_pad([], count($array), '?');

        $query = 'INSERT INTO '.$this->table.' ('.implode(",",$keys).') VALUES ('.implode(",", $binds).')';

        $this->execute($query, $values);

        return true;
    }

    //FAZ O FETCH DE DADOS DO BANCO
    public function select($where=null, $order=null, $limit=null, $what = '*') {
        $where = isset($where) ? ' WHERE '.$where : '';
        $order = isset($order) ? ' ORDER BY '.$order : '';
        $limit = isset($limit)  ? ' LIMIT BY '.$limit : '';

        $query = 'SELECT '.$what. ' FROM '. $this->table. $where.$order.$limit;

        return $this->execute($query);


    }

    //FUNÇÃO QUE ATUALIZA DADOS DO BANCO
    public function update($array, $where) {

        $keys = array_keys($array);
        $values = array_values($array);

        $query = 'UPDATE '.$this->table .' SET '.implode("=?,", $keys).'=? where '.$where;

        $this->execute($query, $values);

        return true;


    }

    //FUNÇÃO RESPONSÁVEL POR DELETAR REGISTROS DO BANCO
    public function delete($where) {
        $query = 'DELETE FROM '.$this->table. ' WHERE '.$where;

        $this->execute($query);

        return true;
    }


}



?>