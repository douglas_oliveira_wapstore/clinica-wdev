<?php 

namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Clinica {



    //NOME DA CLÍNICA
    public $nome;

    //ENDEREÇO DA CLÍNICA
    public $endereco;


    //FUNÇÃO QUE CADASTRA A CLÍNICA NO BANCO
    public function cadastrar() {
        $obDatabase = new Database('clinica');

        $values = [
            'nome' => $this->nome,
            'endereco' => $this->endereco
        ];

        $obDatabase->insert($values);

        return true;
    }

        //função responsável por obter as clínicas do banco
        public static function getClinicas($where = null, $order = null, $limit = null, $what = '*') {
            return (new Database('clinica'))->select($where, $order, $limit, $what)->fetchAll(PDO::FETCH_CLASS, self::class);
        }

        //função que obtém uma clínica específica do banco, baseando-se em seu id
        public static function getclinbyid($id) {
            return (new Database('clinica'))->select('idclinica = '.$id)->fetchObject(self::class);
        }


        
        //função responsável por deletar dados do banco
    public static function apagar($id) {
        return (new Database('clinica'))->delete('id ='. $id);
}



}



?>