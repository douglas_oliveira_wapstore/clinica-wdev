<?php 

namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Especialidade {

    //IDENTIFICADOR DA ESPECIALIDADE
    public $id;

    //NOME DA CLÍNICA
    public $nome;

    //ENDEREÇO DA CLÍNICA
    public $descricao;


    //FUNÇÃO QUE CADASTRA A CLÍNICA NO BANCO
    public function cadastrar() {
        $obDatabase = new Database('especialidade');

        $values = [
            'nome' => $this->nome,
            'descricao' => $this->descricao
        ];

        $obDatabase->insert($values);

        return true;
    }

    //função responsável por obter as especialidades do banco
    public static function getEspecialidades($where = null, $order = null, $limit = null, $what = '*') {
        return (new Database('especialidade'))->select($where, $order, $limit, $what)->fetchAll(PDO::FETCH_CLASS, self::class);
    }

    //função que obtém uma clínica específica do banco, baseando-se em seu id
    public static function getespbyid($id) {
        return (new Database('especialidade'))->select('idespecialidade = '.$id)->fetchObject(self::class);
    }
    
    


}