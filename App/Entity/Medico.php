<?php 

namespace App\Entity;

use \App\Db\Database;
use \PDO;

class Medico {


    //IDENTIFICADOR DO MÉDICO
    public $id;

    //NOME DA CLÍNICA
    public $nome;

    //CRM ÚNICO DO MÉDICO
    public $crm;

    //IDADE DO MÉDICO
    public $idade;

    //SEXO
    public $sexo;

    //IDENTIFICADOR DA ESPECIALIDADE DO MÉDICO
    public $idespecialidade;

    //IDENTIFICADOR DA CLÍNICA DO MÉDICO
    public $idclinica;


    //FUNÇÃO QUE CADASTRA A CLÍNICA NO BANCO
    public function cadastrar() {
        $obDatabase = new Database('medico');

        $values = [
            'nome' => $this->nome,
            'crm' => $this->crm,
            'idade' => $this->idade,
            'sexo' => $this->sexo,
            'idespecialidade' => $this->idespecialidade,
            'idclinica' => $this->idclinica
        ];

        $this->id = $obDatabase->insert($values);

        return true;
    }

    //FUNÇÃO QUE ATUALIZA DADOS DO MÉDICO
    public function atualizar($id) {
        $obDatabase = new Database('medico');

        $values = [
            'nome' => $this->nome,
            'crm' => $this->crm,
            'idade' => $this->idade,
            'sexo' => $this->sexo,
            'idespecialidade' => $this->idespecialidade,
            'idclinica' => $this->idclinica
        ];

        $obDatabase->update($values, 'id = '.$id);

        return true;
    }

     //função responsável por obter os médicos do banco
     public static function getMeds($where = null, $order = null, $limit = null, $what = '*') {
         return (new Database('medico'))->select($where, $order, $limit, $what)->fetchAll(PDO::FETCH_CLASS, self::class);
     }


     
        //função que obtém uma clínica específica do banco, baseando-se em seu id
        public static function getmedbyid($id) {
            return (new Database('medico'))->select('id = '.$id)->fetchObject(self::class);
        }

        //função responsável por deletar dados do banco
    public static function apagar($id) {
            return (new Database('medico'))->delete('id ='. $id);
    }

    


}
