<?php 

require __DIR__.'./vendor/autoload.php';

use \App\Entity\Clinica;

if(isset($_POST['enviar'])){
    if(isset($_POST['nome'], $_POST['endereco'])) {
        $obClinica = new Clinica();

        $obClinica->nome = $_POST['nome'];
        $obClinica->endereco = $_POST['endereco'];

        $obClinica->cadastrar();
        

        header('location: index.php?status=success');
        exit;
    }

}


include __DIR__.'./includes/form-cad-clinica.php';

?>